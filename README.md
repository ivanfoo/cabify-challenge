# cabify devops challenge

These playbooks will provision a nginx proxy and a simple ruby app on a base 
image of ubuntu 16.04 server. It assumes that there is a *sudo-capable* user 
(no password prompt) that can access the target server(s) by `ssh` using a rsa key.

### Notes

- The target hosts are [proxies], specified in inventory/challenge: **fill it before provisioning!**
- The default ssh user is `devops`
- The default ssh private key is `~/.ssh/cabify_rsa`
- Ubuntu 16.04 comes without python2 installed...so I included a little fix

### Nginx

The `proxy` role installs and configures nginx to relay requests to real 
cabify servers. It supports static content caching and SSL, using self-signed 
certifcates proviously generated (I used `openssl`).

Use example:

`curl -k https://<server_address>/api/regions`

### Meter

The `meter` role installs a little nginx monitoring tool called 
[meter](https://github.com/ivanfoo/meter), using `nginx` and `thin` to serve it.  

To access it: `http://<sever_address>:8000/metrics`

### Provisioning

ansible-playbook [-i ssh-private-key] [-u user] challenge.yml

The recipes are intended to be idempotent, so feel free to run them as 
much as you need.

